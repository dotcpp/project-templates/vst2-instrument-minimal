#include "minimal.h"

#include <cstdlib>
#include <ctime>
#include <string.h>

//-------------------------------------------------------------------------------------------------------
AudioEffect *createEffectInstance(audioMasterCallback audioMaster)
{
    return new Minimal(audioMaster);
}

//-------------------------------------------------------------------------------------------------------
Minimal::Minimal(audioMasterCallback audioMaster)
    : AudioEffectX(audioMaster, 1, 1) // 1 program, 1 parameter only
{
    /* Generate a new random seed from system time - do this once in your constructor */
    srand(time(0));

    setNumInputs(2);       // stereo in
    setNumOutputs(2);      // stereo out
    setUniqueID('Nois');   // identify
    canProcessReplacing(); // supports replacing output
    canDoubleReplacing();  // supports double precision processing

    // This makes it a sound generating plugin
    cEffect.flags |= effFlagsIsSynth;

    _gain = 0.5f;                                           // default to 0 dB
    strcpy_s(_programName, kVstMaxProgNameLen, "Default"); // default program name
}

//-------------------------------------------------------------------------------------------------------
Minimal::~Minimal()
{
    // nothing to do here
}

//-------------------------------------------------------------------------------------------------------
void Minimal::setProgramName(char *name)
{
    strcpy_s(_programName, kVstMaxProgNameLen, name);
}

//-----------------------------------------------------------------------------------------
void Minimal::getProgramName(char *name)
{
    strcpy_s(name, kVstMaxProgNameLen, _programName);
}

//-----------------------------------------------------------------------------------------
void Minimal::setParameter(VstInt32 index, float value)
{
    _gain = value;
}

//-----------------------------------------------------------------------------------------
float Minimal::getParameter(VstInt32 index)
{
    return _gain;
}

//-----------------------------------------------------------------------------------------
void Minimal::getParameterName(VstInt32 index, char *label)
{
    strcpy_s(label, kVstMaxParamStrLen, "Gain");
}

//-----------------------------------------------------------------------------------------
void Minimal::getParameterDisplay(VstInt32 index, char *text)
{
    dB2string(_gain, text, kVstMaxParamStrLen);
}

//-----------------------------------------------------------------------------------------
void Minimal::getParameterLabel(VstInt32 index, char *label)
{
    strcpy_s(label, kVstMaxParamStrLen, "dB");
}

//------------------------------------------------------------------------
bool Minimal::getEffectName(char *name)
{
    strcpy_s(name, kVstMaxEffectNameLen, "Whitenoise");
    return true;
}

//------------------------------------------------------------------------
bool Minimal::getProductString(char *text)
{
    strcpy_s(text, kVstMaxProductStrLen, "Whitenoise");
    return true;
}

//------------------------------------------------------------------------
bool Minimal::getVendorString(char *text)
{
    strcpy_s(text, kVstMaxVendorStrLen, "dotcpp");
    return true;
}

//-----------------------------------------------------------------------------------------
VstInt32 Minimal::getVendorVersion()
{
    return 1000;
}

/* Setup constants */
const static int q = 15;
const static float c1 = (1 << q) - 1;
const static float c2 = ((int)(c1 / 3)) + 1;
const static float c3 = 1.f / c1;

//-----------------------------------------------------------------------------------------
void Minimal::processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames)
{
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    /* random number in range 0 - 1 not including 1 */
    float random = 0.f;

    /* the white noise */
    float noise = 0.f;

    while (--sampleFrames >= 0)
    {
        random = ((float)rand() / (float)(RAND_MAX + 1));
        noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
        (*out1++) = noise * _gain;

        random = ((float)rand() / (float)(RAND_MAX + 1));
        noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
        (*out2++) = noise * _gain;
    }
}

//-----------------------------------------------------------------------------------------
void Minimal::processDoubleReplacing(double **inputs, double **outputs, VstInt32 sampleFrames)
{
    double *out1 = outputs[0];
    double *out2 = outputs[1];
    double dGain = _gain;

    /* random number in range 0 - 1 not including 1 */
    double random = 0.f;

    /* the white noise */
    double noise = 0.f;

    while (--sampleFrames >= 0)
    {
        random = ((double)rand() / (double)(RAND_MAX + 1));
        noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
        (*out1++) = noise * dGain;

        random = ((double)rand() / (double)(RAND_MAX + 1));
        noise = (2.f * ((random * c2) + (random * c2) + (random * c2)) - 3.f * (c2 - 1.f)) * c3;
        (*out2++) = noise * dGain;
    }
}
